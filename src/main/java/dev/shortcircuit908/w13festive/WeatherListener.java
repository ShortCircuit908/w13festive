package dev.shortcircuit908.w13festive;

import org.bukkit.WeatherType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class WeatherListener implements Listener {
	@EventHandler
	public void falseSnow(final PlayerJoinEvent event) {
		event.getPlayer().setPlayerWeather(WeatherType.DOWNFALL);
	}
	
	@EventHandler
	public void ensureFalseSnow(final PlayerChangedWorldEvent event) {
		event.getPlayer().setPlayerWeather(WeatherType.DOWNFALL);
	}
}
