package dev.shortcircuit908.w13festive.thaw;

import dev.shortcircuit908.w13festive.W13Festive;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Snow;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class QueueThawTask implements Runnable {
	public static final Map<UUID, Queue<WorldCoordinate>> WORLD_THAW_QUEUE = new HashMap<>();
	private final int max_offset;
	private final Map<UUID, Set<WorldCoordinate>> global_pre_queue = new HashMap<>();
	private final boolean shuffle;
	private final W13Festive plugin;
	private final AtomicBoolean sweep_done = new AtomicBoolean(true);
	private final boolean skip_unloaded;
	
	public QueueThawTask(W13Festive plugin) {
		this.plugin = plugin;
		this.max_offset = Math.abs(plugin.getConfig().getInt("thaw.queue-search-radius", 32));
		this.shuffle = plugin.getConfig().getBoolean("thaw.randomize-queue", true);
		this.skip_unloaded = plugin.getConfig().getBoolean("thaw.queue-skips-unloaded-chunks", false);
	}
	
	@Override
	public void run() {
		if (!sweep_done.getAndSet(false)) {
			return;
		}
		global_pre_queue.values().forEach(Set::clear);
		
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			Set<WorldCoordinate> pre_queue = global_pre_queue.computeIfAbsent(
					player.getWorld().getUID(),
					(uuid) -> new HashSet<>(
							(max_offset * 2 + 1) * (max_offset * 2 + 1) * plugin.getServer().getOnlinePlayers().size())
			);
			
			Location player_location = player.getLocation();
			int pos_x = player_location.getBlockX();
			int pos_z = player_location.getBlockZ();
			for (int x = -max_offset; x <= max_offset; x++) {
				for (int z = -max_offset; z <= max_offset; z++) {
					if (skip_unloaded && !player.getWorld().isChunkLoaded((pos_x + x) >> 4, (pos_z + z) >> 4)) {
						continue;
					}
					Block block = player.getWorld().getHighestBlockAt(pos_x + x, pos_z + z).getRelative(BlockFace.UP);
					if (!block.getType().equals(Material.SNOW) || shouldSnowStay(block)) {
						continue;
					}
					pre_queue.add(new WorldCoordinate(block.getX(), block.getY(), block.getZ()));
				}
			}
		}
		
		for (Map.Entry<UUID, Set<WorldCoordinate>> entry : global_pre_queue.entrySet()) {
			// Get queue for world
			Queue<WorldCoordinate> thaw_queue =
					WORLD_THAW_QUEUE.computeIfAbsent(entry.getKey(), (uuid -> new ConcurrentLinkedQueue<>()));
			Collection<WorldCoordinate> pre_queue = entry.getValue();
			
			// Shuffle world queue if enabled
			if (shuffle) {
				List<WorldCoordinate> ordered = new ArrayList<>(pre_queue);
				Collections.shuffle(ordered);
				pre_queue = ordered;
			}
			
			thaw_queue.addAll(pre_queue);
		}
		
		sweep_done.set(true);
	}
	
	public static boolean shouldSnowStay(Block block) {
		Biome biome = block.getBiome();
		int y_coordinate = block.getY();
		switch (biome) {
			case FROZEN_OCEAN:
			case FROZEN_RIVER:
			case SNOWY_TUNDRA:
			case SNOWY_MOUNTAINS:
			case SNOWY_BEACH:
			case SNOWY_TAIGA:
			case SNOWY_TAIGA_HILLS:
			case DEEP_FROZEN_OCEAN:
			case ICE_SPIKES:
			case SNOWY_TAIGA_MOUNTAINS:
				return true;
			case STONE_SHORE:
			case MOUNTAINS:
			case MOUNTAIN_EDGE:
			case MODIFIED_GRAVELLY_MOUNTAINS:
			case GRAVELLY_MOUNTAINS:
			case TAIGA_MOUNTAINS:
			case WOODED_MOUNTAINS:
				return y_coordinate >= 90;
			case TAIGA:
			case TAIGA_HILLS:
			case GIANT_TREE_TAIGA:
			case GIANT_TREE_TAIGA_HILLS:
				return y_coordinate >= 120;
			case GIANT_SPRUCE_TAIGA:
			case GIANT_SPRUCE_TAIGA_HILLS:
				return y_coordinate > 160;
			default:
				break;
		}
		BlockData data = block.getBlockData();
		if (data instanceof Snow) {
			return ((Snow) data).getLayers() > 1;
		}
		return false;
	}
}
