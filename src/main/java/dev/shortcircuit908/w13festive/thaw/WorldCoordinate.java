package dev.shortcircuit908.w13festive.thaw;

public class WorldCoordinate {
	public final int x;
	public final int y;
	public final int z;
	
	public WorldCoordinate(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public int hashCode() {
		int result = x ^ (x >>> 16);
		result = 31 * result + (y ^ (y >>> 16));
		result = 31 * result + (z ^ (z >>> 16));
		return result;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof WorldCoordinate)) {
			return false;
		}
		WorldCoordinate cast = (WorldCoordinate) o;
		return cast.x == x && cast.y == y && cast.z == z;
	}
}
