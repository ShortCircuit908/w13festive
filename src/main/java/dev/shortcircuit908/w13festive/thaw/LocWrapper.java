package dev.shortcircuit908.w13festive.thaw;

import org.bukkit.Location;

import java.util.Objects;

public class LocWrapper {
	private final Location location;
	
	public LocWrapper(Location location) {
		this.location = location;
	}
	
	public Location getLocation() {
		return location;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(location.getWorld() == null ? null : location.getWorld().getUID(),
				location.getBlockX(),
				location.getBlockY(),
				location.getBlockZ()
		);
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof LocWrapper)) {
			return false;
		}
		Location o_loc = ((LocWrapper) other).location;
		return Objects.equals(location.getWorld(), o_loc.getWorld()) && location.getBlockX() == o_loc.getBlockX() &&
				location.getBlockY() == o_loc.getBlockY() && location.getBlockZ() == o_loc.getBlockZ();
	}
}
