package dev.shortcircuit908.w13festive.thaw;

import dev.shortcircuit908.w13festive.W13Festive;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class ThawTask implements Runnable {
	private final W13Festive plugin;
	private final int blocks_to_process;
	private static final AtomicBoolean sweep_done = new AtomicBoolean(true);
	private final Map<UUID, Queue<WorldCoordinate>> local_queues = new HashMap<>();
	private final boolean skip_unloaded;
	
	public ThawTask(W13Festive plugin) {
		this.plugin = plugin;
		this.blocks_to_process = plugin.getConfig().getInt("thaw.blocks-per-thaw-operation", 8);
		this.skip_unloaded = plugin.getConfig().getBoolean("thaw.thaw-skips-unloaded-chunks", false);
	}
	
	@Override
	public void run() {
		// Process raw queue
		long queued = 0;
		for (Map.Entry<UUID, Queue<WorldCoordinate>> raw_queue : QueueThawTask.WORLD_THAW_QUEUE.entrySet()) {
			Queue<WorldCoordinate> local_queue =
					local_queues.computeIfAbsent(raw_queue.getKey(), (uuid) -> new ConcurrentLinkedQueue<>());
			WorldCoordinate coordinate;
			// Remove all available elements from raw queue
			while ((coordinate = raw_queue.getValue().poll()) != null) {
				// Add unique coordinate to local queue
				if (!local_queue.contains(coordinate)) {
					local_queue.add(coordinate);
					queued++;
				}
			}
		}
		if (queued > 0) {
			plugin.getLogger().info("Queued " + queued + " block" + (queued == 1 ? "" : "s") + " for thawing");
		}
		
		if (!sweep_done.getAndSet(false)) {
			return;
		}
		
		Block block;
		World world;
		WorldCoordinate coordinate;
		Iterator<Map.Entry<UUID, Queue<WorldCoordinate>>> iterator = local_queues.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<UUID, Queue<WorldCoordinate>> entry = iterator.next();
			world = Bukkit.getServer().getWorld(entry.getKey());
			if (world == null) {
				iterator.remove();
				continue;
			}
			try {
				for (int i = 0; i < blocks_to_process; i++) {
					coordinate = entry.getValue().remove();
					if (coordinate == null ||
							(skip_unloaded && !world.isChunkLoaded(coordinate.x >> 4, coordinate.z >> 4))) {
						continue;
					}
					
					block = world.getBlockAt(coordinate.x, coordinate.y, coordinate.z);
					if (!block.getType().equals(Material.SNOW) || QueueThawTask.shouldSnowStay(block)) {
						continue;
					}
					block.setType(Material.AIR, true);
				}
			}
			catch (NoSuchElementException e) {
				// Do nothing
			}
		}
		sweep_done.set(true);
	}
}
