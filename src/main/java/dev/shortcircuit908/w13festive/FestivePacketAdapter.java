package dev.shortcircuit908.w13festive;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.utility.MinecraftReflection;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import org.bukkit.block.Biome;
import org.bukkit.configuration.ConfigurationSection;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class FestivePacketAdapter extends PacketAdapter {
	private final W13Festive plugin;
	private final Map<Integer, Integer> biome_key_remappings;
	
	public FestivePacketAdapter(W13Festive plugin) {
		super(plugin, PacketType.Play.Server.MAP_CHUNK);
		this.plugin = plugin;
		this.biome_key_remappings =
				new Int2IntOpenHashMap(getBiomeKeyRemappings(getBiomeRemappings(), getBiomeKeyMappings()));
	}
	
	@Override
	public void onPacketSending(PacketEvent event) {
		PacketContainer container = event.getPacket();
		int[] value = container.getIntegerArrays().read(0);
		
		for (int i = 0; i < value.length; i++) {
			value[i] = getRemappedBiomeKey(value[i]);
		}
	}
	
	private int getRemappedBiomeKey(int key) {
		return biome_key_remappings.getOrDefault(key, key);
	}
	
	@SuppressWarnings("unchecked")
	private EnumMap<Biome, Integer> getBiomeKeyMappings() {
		EnumMap<Biome, Integer> bukkit_mapped = new EnumMap<>(Biome.class);
		
		try {
			// Gather required NMS classes
			Class<?> class_BiomeRegistry = MinecraftReflection.getMinecraftClass("BiomeRegistry");
			Class<?> class_ResourceKey = MinecraftReflection.getMinecraftClass("ResourceKey");
			Class<?> class_MinecraftKey = MinecraftReflection.getMinecraftClass("MinecraftKey");
			
			// Field containing the String name of a MinecraftKey
			Field field_MinecraftKey_key = class_MinecraftKey.getDeclaredField("key");
			field_MinecraftKey_key.setAccessible(true);
			
			// Field containing the MinecraftKey of a ResourceKey
			Field field_ResourceKey_c = class_ResourceKey.getDeclaredField("c");
			field_ResourceKey_c.setAccessible(true);
			
			// Field containing the Int2ObjectMap<ResourceKey> of the BiomeRegistry
			Field field_biome_map = class_BiomeRegistry.getDeclaredField("c");
			field_biome_map.setAccessible(true);
			
			Map<Integer, ?> biome_map = (Map<Integer, ?>) field_biome_map.get(null);
			
			for (Map.Entry<Integer, ?> entry : biome_map.entrySet()) {
				// Get the integer ID of the biome
				int key = entry.getKey();
				// Get the name of the biome
				Object resource_key = entry.getValue();
				Object minecraft_key = field_ResourceKey_c.get(resource_key);
				String namespaced_key = minecraft_key.toString();
				Biome biome = getBiome(namespaced_key);
				if (biome == null) {
					plugin.getLogger()
							.warning("Failed to resolve Minecraft biome " + namespaced_key + " to Bukkit biome");
					continue;
				}
				// Map the biome to the integer key
				bukkit_mapped.put(biome, key);
			}
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		
		return bukkit_mapped;
	}
	
	private Biome getBiome(String namespaced_key) {
		for (Biome biome : Biome.values()) {
			if (biome.getKey().toString().equalsIgnoreCase(namespaced_key)) {
				return biome;
			}
		}
		return null;
	}
	
	private Biome getBiomeEnthusiastically(String name) {
		Biome biome = getBiome(name);
		if (biome == null) {
			for (Biome check : Biome.values()) {
				if (check.name().equalsIgnoreCase(name)) {
					biome = check;
					break;
				}
			}
		}
		return biome;
	}
	
	private EnumMap<Biome, Biome> getBiomeRemappings() {
		EnumMap<Biome, Biome> remappings = new EnumMap<>(Biome.class);
		Map<String, Object> raw_remappings = new HashMap<>();
		
		ConfigurationSection mappings_section = plugin.getConfig().getConfigurationSection("biome-mappings");
		if (mappings_section == null || (raw_remappings = mappings_section.getValues(false)).isEmpty()) {
			plugin.getLogger().warning("Biome remapping config is empty, using default remappings");
			try {
				raw_remappings = plugin.getDefaultConfig().getConfigurationSection("biome-mappings").getValues(false);
			}
			catch (IOException | IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		else {
			raw_remappings = mappings_section.getValues(false);
		}
		for (Map.Entry<String, Object> entry : raw_remappings.entrySet()) {
			Biome from = getBiomeEnthusiastically(entry.getKey());
			Biome to = getBiomeEnthusiastically(entry.getValue() == null ? null : entry.getValue().toString());
			if (from == null) {
				plugin.getLogger().warning("Unknown biome in remapping config: " + entry.getKey());
			}
			if (to == null) {
				plugin.getLogger().warning("Unknown biome in remapping config: " + entry.getValue());
			}
			if (from == null || to == null) {
				continue;
			}
			remappings.put(from, to);
		}
		
		return remappings;
	}
	
	private Map<Integer, Integer> getBiomeKeyRemappings(Map<Biome, Biome> biome_remappings,
														Map<Biome, Integer> biome_key_map) {
		Map<Integer, Integer> remapped = new HashMap<>(biome_remappings.size());
		for (Map.Entry<Biome, Biome> biome_remapper : biome_remappings.entrySet()) {
			// Map new biome ID to old biome ID
			int from = biome_key_map.get(biome_remapper.getKey());
			int to = biome_key_map.get(biome_remapper.getValue());
			plugin.getLogger().info(String.format(
					"Remapping %1$s (ID %2$s) to %3$s (ID %4$s)",
					biome_remapper.getKey().getKey(),
					from,
					biome_remapper.getValue().getKey(),
					to
			));
			remapped.put(from, to);
		}
		return remapped;
	}
}
