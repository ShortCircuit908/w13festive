package dev.shortcircuit908.w13festive;

import com.comphenix.protocol.ProtocolLibrary;
import dev.shortcircuit908.w13festive.thaw.QueueThawTask;
import dev.shortcircuit908.w13festive.thaw.ThawTask;
import org.bukkit.WeatherType;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.dependency.Dependency;
import org.bukkit.plugin.java.annotation.plugin.ApiVersion;
import org.bukkit.plugin.java.annotation.plugin.LogPrefix;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

@Plugin(name = "w13festive", version = "1.1.0")
@ApiVersion(ApiVersion.Target.v1_15)
@Author("ShortCircuit908")
@LogPrefix("W13F")
@Dependency("ProtocolLib")
public class W13Festive extends JavaPlugin {
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		
		try {
			getConfig().setDefaults(getDefaultConfig());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		if (getConfig().getBoolean("remap-biomes")) {
			ProtocolLibrary.getProtocolManager().addPacketListener(new FestivePacketAdapter(this));
		}
		if (getConfig().getBoolean("always-snowing")) {
			getServer().getPluginManager().registerEvents(new WeatherListener(), this);
			
			for (Player player : getServer().getOnlinePlayers()) {
				player.setPlayerWeather(WeatherType.DOWNFALL);
			}
		}
		if (getConfig().getBoolean("thaw-snow")) {
			getServer().getScheduler()
					.scheduleAsyncRepeatingTask(this,
							new QueueThawTask(this),
							0,
							getConfig().getInt("thaw.ticks-between-queue-operations", 100)
					);
			getServer().getScheduler()
					.scheduleSyncRepeatingTask(this,
							new ThawTask(this),
							0,
							getConfig().getInt("thaw.ticks-between-thaw-operations", 1)
					);
		}
	}
	
	@Override
	public void onDisable() {
		ProtocolLibrary.getProtocolManager().removePacketListeners(this);
		getServer().getScheduler().cancelTasks(this);
	}
	
	public Configuration getDefaultConfig() throws IOException, IllegalArgumentException {
		try (InputStream in = getResource("config.yml")) {
			if (in == null) {
				throw new IllegalArgumentException(
						"The embedded resource 'config.yml' cannot be found in " + getFile());
			}
			try (Reader reader = new InputStreamReader(in)) {
				return YamlConfiguration.loadConfiguration(reader);
			}
		}
	}
}
